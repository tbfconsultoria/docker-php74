### Criar certificado

1. Instale o programa [mkcert](https://github.com/FiloSottile/mkcert)

2. Execute o comando para os domínios que deseja criar.

```shell
$ mkcert dev.site.com.br localhost 127.0.0.1 ::1
Created a new local CA 💥
Note: the local CA is not installed in the system trust store.
Run "mkcert -install" for certificates to be trusted automatically ⚠️

Created a new certificate valid for the following names 📜
 - "dev.site.com.br"
 - "localhost"
 - "127.0.0.1"
 - "::1"

The certificate is at "./dev.site.com.br+3.pem" and the key at "./dev.site.com.br+3-key.pem" ✅

It will expire on 23 October 2023 🗓
```

3. Instale no seu browser.

```shell
$ mkcert -install
```

4. Renomeie os arquivos que no exemplo acima são **dev.site.com.br.pem** e **dev.site.com.br-key.pem**, para:  

        dev.site.com.br.pem > cert.pem
        dev.site.com.br-key.pem > cert-key.pem

5. Veja o caminho do arquivo **rootCA.pem** executando:

```shell
$ mkcert -CAROOT
```

6. Copie os arquivos (cert.pem, cert-key.pem, rootCA.pem) para a pasta `docker\provision\ssl`

7. Descomente as configurações do SSL no **VirtualHost** em `docker\provision\vhost.conf`

8. Faça o Re-Faça o build do docker:

```shell
$ docker-compose up -d --build
```