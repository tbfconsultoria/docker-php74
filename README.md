# Docker
---

> * Quando a data/hora dos containers não estiver correta, execute: `$ wsl --shutdown`  
> * **SSL** se for usar, descomente as linhas 47,48 do **Dockerfile** e leia o [MANUAL](/provision/ssl/MANUAL.md)

Este docker possui:  

* PHP 7.4.23
* MySQL 8.0.25
* APACHE 2.4
* Composer
* [MailHog](http://localhost:8025/)
* [WP-CLI](https://make.wordpress.org/cli/handbook/guides/quick-start/)

Com a estrutura de pasta

| Pasta | Descrição |
| ------ | ------ |
| provision/  | Contendo os arquivos de configuração |
| www         | Pasta das aplicações |

### MySQL
---

Dados para configurar conexão nas aplicações PHP:  
> No **MySQL Workbench** o **HOST** use como `localhost` ou `127.0.0.1`

```plain
Host: php74_mysql:3306
User: root
Pass: root
```
        
### SMTP
---

Dados de conexão:  

```plain
Host: php74_mailhog:1025
User: null
Pass: null
```

### WordPress
---

Como baixar e instalar o wordpress via WP-CLI

1. Faça o download:

```shell
# No PATH, a pasta de exemplo "site" será criada se ela não existir
$ wp core download --locale=pt_BR --path=/var/www/site
```

2. Crie e configure o `wp-config.php`

```shell
$ cd /var/www/site

# Altere o nome do banco em "--dbname"
$ wp config create --dbname=meubanco --dbuser=root --dbpass=root --dbhost=php74_mysql:3306
```