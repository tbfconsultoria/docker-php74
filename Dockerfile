FROM php:7.4-apache

RUN apt-get update; \
    apt-get install -y --no-install-recommends \
    apt-utils \
    ghostscript \
    less \
    nano \
    default-mysql-client \
    libfreetype6-dev \
    libjpeg-dev \
    libpng-dev \
    libzip-dev \
    libxml2-dev \
    cron \
    locales \
    && rm -rf /var/lib/apt/lists/* 

RUN sed -i 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/g' /etc/locale.gen
RUN echo "LANG=pt_BR.UTF-8" >> /etc/default/locale
RUN locale-gen
RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
ENV LC_ALL pt_BR.UTF-8
ENV LANG pt_BR.UTF-8
ENV LANGUAGE pt_BR.UTF-8

RUN docker-php-ext-configure gd \
    --with-freetype=/usr \
    --with-jpeg=/usr \
    && docker-php-ext-enable opcache \
    && docker-php-ext-install \
    bcmath \
    exif \
    gd \
    mysqli \
    pdo_mysql \
    zip \
    soap

# alguns sistemas usam essas extensões com por exemplo "moodle"
#RUN docker-php-ext-install intl xmlrpc

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN mv wp-cli.phar /usr/local/bin/wp && chmod +x /usr/local/bin/wp

# SSL, descomente as 2 linhas abaixo se for usar
#RUN mkdir -p /etc/apache2/ssl
#COPY ./provision/ssl/cert*.pem  /etc/apache2/ssl/

WORKDIR /var/www

RUN a2enmod rewrite ssl
RUN service apache2 restart
RUN service cron restart

USER www-data